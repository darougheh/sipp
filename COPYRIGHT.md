Copyright and acknowledgements
==============================

This code was derived from the Center for Economic and Policy Research (CEPR) 
uniform extracts of the SIPP data. For a detailed copyright notice and
acknowledgements, see the comments in the original files posted on the 
[CEPR SIPP site] [cepr].

The Stata dictionaries and value label definitions are based on code released 
by the NBER. The copyright for this code lies with Jean Roth (and possibly
others) at the NBER. See the comments in these files at the [NBER's SIPP site] [nber] 
for details.

[cepr]: http://ceprdata.org/sipp-uniform-data-extracts/ "CEPR uniform extracts"
[nber]: http://www.nber.org/data/sipp.html "NBER's SIPP page"

For all other copyrights, see the comments at the beginning of each file.

License
=======

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Copies of the GNU GPL version 2 and 3 can be found in the files
LICENSE-GPLv2.txt and LICENSE-GPLv3.txt, respectively,
in the root directory of this distribution.
