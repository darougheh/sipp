// Defines value labels for ethnic origin.
// Should be -include-d into sipp_process_b.do

// FIXME: CEPR copyright
// FIXME: licensing

if `panel' <= 1993 {
    
    #delimit ;
    label define lbl_ethnic 
        1   "German" 
        2   "English" 
        3   "Irish" 
        4   "French" 
        5   "Italian" 
        6   "Scottish" 
        7   "Polish" 
        8   "Dutch" 
        9   "Swedish" 
        10  "Norwegian" 
        11  "Russian" 
        12  "Ukranian" 
        13  "Welsh" 
        14  "Mexican-American" 
        15  "Chicano" 
        16  "Mexican" 
        17  "Puerto Rican"
        18  "Cuban" 
        19  "Central or South American (Spanish)" 
        20  "Other Spanish" 
        21  "Afro-American" 
        30  "Other"
        39  "Don't know"
        ;
    #delimit cr
}
else if `panel' == 1996 | `panel' == 2001 {
    
    #delimit ;
    label define lbl_ethnic 
        1   "Canadian" 
        2   "Dutch" 
        3   "English" 
        4   "French" 
        5   "French-Canadian" 
        6   "German" 
        7   "Hungarian" 
        8   "Irish" 
        9   "Italian" 
        10  "Polish" 
        11  "Russian" 
        12  "Scandinavian" 
        13  "Scotch-Irish" 
        14  "Scottish" 
        15  "Slovak" 
        16  "Welsh" 
        17  "Other European" 
        20  "Mexican" 
        21  "Mexican-American" 
        22  "Chicano" 
        23  "Puerto Rican" 
        24  "Cuban" 
        25  "Central American" 
        26  "South American" 
        27  "Dominican Republic" 
        28  "Other Hispanic" 
        30  "African-American or Afro-American" 
        31  "American Indian, Eskimo, or Aleut" 
        32  "Arab" 
        33  "Asian" 
        34  "Pacific Islander" 
        35  "West Indian" 
        39  "Another group not listed" 
        40  "American"
        ;
    #delimit cr
}
else if `panel' == 2004 | `panel' == 2008 {
    label define lbl_ethnic 1 "Spanish, Hispanic or Latino" 2 "Not Spanish or Hispanic or Latino"
}

// vim: sts=4 sw=4 et:
