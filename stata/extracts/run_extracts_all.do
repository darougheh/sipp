
clear *
set more off
set trace off
pause off

run "sipp_extract_env.do"

capture log close

log using "${log_dir}/sipp-extract-all.log", replace

// Create extracts for these panels
// local panels 1990/1993
local panels 1990/1993 1996 2001 2004 2008

foreach pid of numlist `panels' {
    sipp_set_name, panel(`pid') setname("a")
    local fn_set_a = "${extracts_dir}/`r(fname)'"

    sipp_set_name, panel(`pid') setname("b")
    local fn_set_b = "${extracts_dir}/`r(fname)'"

    sipp_set_name, panel(`pid') setname("d")
    local fn_set_d = "${extracts_dir}/`r(fname)'"

    noisily sipp_extract_a, panel(`pid') saveas("`fn_set_a'")
    noisily sipp_extract_b, panel(`pid') saveas("`fn_set_b'")
    noisily sipp_extract_d, panel(`pid') saveas("`fn_set_d'")
}


log close
