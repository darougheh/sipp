/*
Industry and occupation classification. Note that classification schemes for
2004+ panels differ from previous ones. For these panels, the Census 2000 to
1990 industry classification crosswalk is used to create the consistent
variable ind_c90 using the Census 1990 system.

This file should be -run- from sipp_process_d.do

TODO: Create consistent variable for occupation.
*/

// Author: Richard Foltyn
// FIXME: CEPR copyright
// FIXME: licensing

version 12

// argument is 4-digit panel year
args panel

// Industry classification crosswalk
// The 1993 - 2001 industries are categorized according to the Census 1990 classification,
// while for the 2004 and 2008 panels the 2000 Census classification is used.
// Create homogenous industry variable:

if `panel' == 2004 | `panel' == 2008 {
    // recode Census 2000 ind. classification to Census 1990 system
    sipp_ind2000_to_ind1990 ind, generate(ind_c90) 

    // note that all 2000 Census ind. codes are set to missing if
    // no applicable new code was found.
    count if ind != ind_c90 & !missing(ind) & !missing(ind_c90)

    tab ind ind_c90 if !missing(ind) & missing(ind_c90), missing
}
else {
    gen ind_c90 = ind
}


// Generate major occupations and industries
if `panel' == 2004 | `panel' == 2008 {
    // Not present in 04 panel due to classification changes
    gen byte occ14 = .
}
else if `panel' <= 2001 {
    gen occ14 = 1 if occ >= 003 & occ <= 37
    replace occ14 = 2 if occ >= 43 & occ <= 199
    replace occ14 = 3 if occ >= 203 & occ <= 235
    replace occ14 = 4 if occ >= 243 & occ <= 285
    replace occ14 = 5 if occ >= 303 & occ <= 389
    replace occ14 = 6 if occ >= 403 & occ <= 407
    replace occ14 = 7 if occ >= 413 & occ <= 427
    replace occ14 = 8 if occ >= 433 & occ <= 469
    replace occ14 = 9 if occ >= 473 & occ <= 499
    replace occ14 = 10 if occ >= 503 & occ <= 699
    replace occ14 = 11 if occ >= 703 & occ <= 799 
    replace occ14 = 12 if occ >= 803 & occ <= 859
    replace occ14 = 13 if occ >= 864 & occ <= 889
    replace occ14 = 14 if occ >= 905 & !missing(occ)
}

generate ind23 = 1 if ind_c90 >= 10 & ind_c90 <= 32
replace ind23 = 2 if ind_c90 >= 40 & ind_c90<50
replace ind23 = 3 if ind_c90 == 60 
replace ind23 = 5 if ind_c90 >= 100 & ind_c90 <= 222
replace ind23 = 4 if ind_c90 >= 230 & ind_c90 <= 392
replace ind23 = 6 if ind_c90 >= 400 & ind_c90 <= 432
replace ind23 = 7 if ind_c90 >= 440 & ind_c90 <= 442
replace ind23 = 8 if ind_c90 >= 450 & ind_c90 <= 472
replace ind23 = 9 if ind_c90 >= 500 & ind_c90 <= 571
replace ind23 = 10 if ind_c90 >= 580 & ind_c90 <= 691
replace ind23 = 11 if ind_c90 >= 700 & ind_c90 <= 712
replace ind23 = 12 if ind_c90 == 761
replace ind23 = 13 if ind_c90 >= 721 & ind_c90 <= 760
replace ind23 = 14 if ind_c90 >= 762 & ind_c90 <= 791
replace ind23 = 15 if ind_c90 >= 800 & ind_c90 <= 810
replace ind23 = 16 if ind_c90 == 831
replace ind23 = 17 if ind_c90 >= 812 & ind_c90 <= 840 & ind_c90 != 831
replace ind23 = 18 if ind_c90 >= 842 & ind_c90 <= 860
replace ind23 = 19 if ind_c90 >= 861 & ind_c90 <= 871
replace ind23 = 20 if ind_c90 == 841 | (ind_c90 >= 872 & ind_c90 <= 893)
replace ind23 = 21 if ind_c90 >= 31 & ind_c90 <= 32
replace ind23 = 22 if ind_c90 >= 900 & ind_c90 <= 932
replace ind23 = 23 if ind_c90 == 991

tab ind23, missing

// tag obs from nonfarm business sector as a lot of studies limit their dataset
// to these.
// We have to differentiate between the 1990-2001 panels which use the 1990 Census industry classification
// and the 2004-2008 panels, which use the 2000 Census industry classification.

generate byte nfarmb = 1 if !missing(ind) & ind > 0

if `panel' <= 2001 {
    // eliminate agriculture, fishing, forestry, etc.
    replace nfarmb = 0 if !missing(ind) & ind > 0 & ind <= 32

    // eliminate public administration
    replace nfarmb = 0 if !missing(ind) & ind >= 900 & ind <= 932

    // active military duty; these should be no obs. with this ind. code as the SIPP
    // exludes military personell
    replace nfarmb = 0 if !missing(ind) & ind >= 940 & ind <= 960 

    // interpretation varies across panels, e.g. persons not employed since 1984
    // in 1990 panel the value is 992
    replace nfarmb = . if ind >= 991
}
else if `panel' == 2004 | `panel' == 2008 {
    // agriculture-related stuff is 0170 - 0290
    replace nfarmb = 0 if !missing(ind) & ind >= 170 & ind <= 290

    // public administration
    // This range also includes military personell with codes 9670 - 9870
    replace nfarmb = 0 if !missing(ind) & ind >= 9370 & ind <= 9870

    // last job was in the military
    replace nfarmb = 0 if ind == 9890

    // not in LF since 1984 (present in 2000 Census class, but should not even exist in SIPP according to DDFs)
    replace nfarmb = . if ind == 9920
}

//------------------------------------------------------------------------------
// Labels / formatting

format %4.0f ind* occ*
format %1.0f nfarmb

label variable occ "Detailed occupation"
label variable occ14 "Occupation, Major Groups"
label variable ind "Detailed industry classification"
label variable ind23 "Industry, Major Groups"
label variable ind_c90  "Census 1990 industry classification"
label variable nfarmb "Indicator variable to tag non-farm private business industry codes"

notes ind: 1990 Census ind. classification for panels up to 2001, 2000 Census classification for ///
    2004/08 panels; 2004/08 ind./occ. not comparable to previous years.
notes occ14: Not available in 2004/08 panels due to ind/occ classification changes
notes occ: 2004/08 ind./occ. not comparable to previous years;

notes nfarmb: "Computed from ind by excluding all agriculute/fishing/forestry/government/army-related industry codes"

//------------------------------------------------------------------------------
// Value labels

// define occupation and industry value labels
do "labels/labels_ind_occ.do"

if `panel' >= 90 & `panel' <= 93 {
    label values ind lbl_ind_census90
    label values occ lbl_occ_detailed
}

label values occ14 lbl_occ14
label values ind23 lbl_ind23
label values ind_c90 lbl_ind_census90


tabulate ind, missing
tabulate nfarmb, missing

// vim: set sts=4 sw=4 et:
