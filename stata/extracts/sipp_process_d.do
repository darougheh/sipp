// File to process (somewhat) harmonized Set D data.
// Note that this file should be run from sipp_extract_b.ado.

// FIXME: CEPR copyright
// FIXME: licensing


/* 
Each SIPP respondent is asked questions about up to two jobs per wave. In order to facilitate easier 
programming, we reshape this into person-job-month, rather than the usual person-month.

Concepts:
Earnings. The concept of earnings used in the SIPP survey differs from that of the Bureau of Labor Statistics 
(BLS). The SIPP survey asks about actual earnings on up to two jobs per month. The self-employed are
also asked about their business earnings, although these are not included in the wage and earnings 
variables. The BLS publishes quarterly averages for an earnings concept called "usual weekly 
earnings" for employed wage and salary workers, which differs from the SIPP
concent since it is based on usual, not actual earnings, excludes the self-employed, and 
excludes earnings from secondary jobs.

Worked each week. Persons "worked each week" in a month if, for the entire month, they were "with a
job" and not "absent without pay" from the job. In other words, a person worked each week in any month
when they were (a) on the job the entire month, or (b) they received wages or a salary for all weeks in the
month, whether they were on the job or not. Persons also worked each week if they were self-employed
and spent time during each week of the month at or on behalf of the business or farm they owned, as long
as they received or expected to receive profit or fees for their work.
In the CPS, the concept at "work" includes those persons who spent at least 1 hour during the reference
week at their job or business. In the CPS, however, "at work" does not include persons who were
temporarily absent from their jobs during the entire reference week on paid vacation, sick leave, etc. In
SIPP, "worked each week" does include persons on paid absences.

With a Job. Persons are classified "with a job" in a given month if they were 16 years old or over and,
during the month, either (a) worked as paid employees or worked in their own business or profession or on
their own farm or worked without pay in a family business or farm or (b) were temporarily absent from work
either with or without pay. In general, the word "job" implies an arrangement for regular work for pay where
payment is in cash wages or salaries, at piece rates, in tips, by commission, or in kind (meals, living
quarters, supplies received). "Job" also includes self-employment at a business, professional practice, or
farm. A business is defined as an activity which involves the use of machinery or equipment in which
money has been invested or an activity requiring an office or "place of business" or an activity which
requires advertising; payment may be in the form of profits or fees.

*/

version 12

// Argument: 4-digit panel year
args panel

//------------------------------------------------------------------------------
// Merge some meta data from Set A that will be used later
dbg_off sipp_set_name, panel(`panel') setname("a")
local fn_set_a = "${extracts_dir}/`r(fname)'"
dbg_off merge 1:1 uuid wave refmth using "`fn_set_a'", ///
    assert(match) nolabel nonotes nogenerate ///
    keepusing(month year rot refdate epppnum)


//------------------------------------------------------------------------------
// Compute start and end date for each wave. We will need this information
// repeatedly below.

preserve

sort rot wave refmth
collapse (first) wave_start=refdate (last) wave_end=refdate, by(rot wave)

tempfile fn_wave_bounds
save "`fn_wave_bounds'", replace

restore

merge m:1 rot wave using "`fn_wave_bounds'", nogenerate nolabel ///
    assert(match)

// N/A jobids are coded as 0
recode job* (min/0 100 = .)

// FIXME: remove in final version
// Make sure we never observe job2 without job1
assert !missing(job1) if !missing(job2)

//------------------------------------------------------------------------------ 
// RESHAPE TO JOB-MONTH FORMAT

// Reshape data to be by job-month, rather than person-month.
// This will create up to two observations per person, per month.

// variables present in all panels
// Note that in the CEPR code selfinc is also included in the reshape.
// This makes no sense as income from up to two businesses is independet of up to two jobs held,
// so we cannot assign selfinc1 to job1 and selfinc2 to job2.
local varlist apmsum class earn wkhours ind job occ rate 

if `panel' >= 1996 {
    local varlist = "`varlist' unionm unionc hrly ersend apyrate estlemp tempall tempsiz tsjdate tejdate asjdate "
}

sort uuid wave refmth
reshape long `varlist', i(uuid wave refmth) j(jobno)

//------------------------------------------------------------------------------ 
// DROP REDUNDANT JOB-MONTH OBS. WITH MISSING JOB ID

// Delete second obs. if both jobs are missing.
// The original CEPR code is unnecessarily complex, and also does more than eliminate
// second missing job: it also eliminates duplicates in terms of 
// (uuid wave refmth job occ and ind).
// Note that there are a up to few thousand such duplicates in some panels, probably due to
// wrong assignment of jobid. These should be dealt with consciously instead of just
// deleting the second observation and pretending that everything is consistent.
tempvar del del2
generate byte `del' = 0
by uuid wave refmth (job), sort: replace `del' = 1 if missing(job[2]) & missing(job[1]) & _n == 2


// Delete second missing job obs. if person has non-missing job 1 or 2 in that period
by uuid wave refmth: replace `del'  = 1 if  _N == 2 & missing(job[_n]) & /// 
    ((!missing(job[_n + 1]) & _n == 1) | (!missing(job[_n - 1])  & _n == 2))

by uuid wave refmth: generate byte `del2' = (_n == 2 & missing(job[2]))

assert `del' == `del2'

drop if `del' == 1

//------------------------------------------------------------------------------ 
// MERGE WAVELY CORE DATA FOR 1990-1993 PANELS

if `panel' <= 1993 {
    do "sipp_set_d_wavely.do" `panel'
}

// ---------------------------------------------------------------------
// RECODING
// Do all recoding except for some earnings-related variables, which will be 
// recoded after topcoded earnings / wages are processed.

do "sipp_set_d_recode.do" `panel'

//------------------------------------------------------------------------------
// JOB START AND END DATES

// compute and clean up start/end dates
do "sipp_set_d_job_dates.do" `panel'

//------------------------------------------------------------------------------
// EMPLOYMENT VARIABLES

/* 

Due to editing of full panel files for 1990-1993 panels, the job ID stored in 
JOB-ID1 is the primary job for this reference month; no further imputation is required.

For 1996+ panels, EENO1 and EENO2 remain constant within waves, even if a
person switches (primary) jobs. The survey instrument asks respondents to
identify for which employers they worked the most and next most hours between
the 1st day of refmth 1 and interview date, and it seems that in the majority
of cases this determines which job ID is stored in EENO1. 
For the moment we assign EENO1 to be primary job; alternatively we could
determine the primary job based on wavely hours worked. The table below lists
how many observations this affects.

*/

sort uuid wave refmth jobno

// ensure that we have primary job even in the case that job with jobno = 1
// was delete in above cleaning procedures
by uuid wave refmth: generate byte primjob = (jobno == 1 | _N == 1) if !missing(job)
drop jobno


if `panel' >= 1996 {
    // if we bothered to determine primary job by wavely hours worked (as seems
    // to be the case in full panel files for pre-1996 panels), this
    // fraction of obs. with two jobs would switch primary job ID.
    tempvar prim_hrs
    by uuid wave refmth: generate byte `prim_hrs' = (wkhours[1] < wkhours[2]) ///
        if _N == 2 & wkhours[1] > 0 & wkhours[2] > 0 & ///
        !missing(wkhours[1]) & !missing(wkhours[2])
    tab `prim_hrs', missing
    drop `prim_hrs'
}

// Emplyoment statuts: either employed, unemployed or missing ESR/RMESR indicates missing value
// Employed at least one week in a month 
gen emp = 1 if esr >= 1 & esr <= 5
// NOTE: there is another definition you may consider: "replace emp=1 if esr == 1 | esr == 2 | esr == 4"
replace emp = 0 if esr >= 6 & esr <= 8

tabulate esr, nolabel missing

generate byte hasjob = !missing(job)

// Tag multiple job holders
// multjob is missing if person has no job, 1 if person has 2 jobs in reference month
// and 0 if person has only 1 job

generate byte multjob = 0 if !missing(job)
by uuid wave refmth, sort: replace multjob = 1 ///
    if  !missing(job[1]) & !missing(job[2])
tabulate multjob, missing nolabel

// the njobs variable is very unreliable (e.g. in takes on the value "Not in universe"
// for all obs in the first 2 waves of the 1990 panel and has illegal values in 1991
// panel). Create our own variable, imputed from present number of jobs.

generate byte njobs_imp = 0
replace njobs_imp = 1 if !missing(job)
replace njobs_imp = 2 if multjob == 1

// In the post-1996 panels, the ejobcntr variable has a completely different interpretation than the
// NJOBS variable in pre-1996 panels. EJOBCNTR reports the number of jobs a worker had during the reference
// period. Hence if a worker reports having more than the two jobs recorded in SIPP, correct the njobs_imp
// variable to reflect this.
if `panel' >= 1996 {
    replace njobs_imp = njobs if njobs > njobs_imp & !missing(njobs)
}

tab2 njobs wave, nolabel missing
tabulate njobs_imp

// Fix paid by hour indicator variable:
// Make sure hrly variable is somewhat consistent with presence of hourly rate.
// In pre-1996 panels, from the skip patterns it follows that hrly = 1 if 
// and only if rate is reported.
// For 1996+ panels, the relationship is not so clear, so we only enforce
// hrly = 1 if rate is nonmissing.

tempvar ihrly1 ihrly0

// This case does not seem to occur in 1996+ panels at all.
generate `ihrly1' = !missing(rate) & hrly != 1 if !missing(job)
replace hrly = 1 if `ihrly1' == 1
tab1 `ihrly1' , missing

if `panel' <= 1993 {
    generate `ihrly0' = missing(rate) & hrly != 0 if !missing(job)
    tab1 `ihrly0', missing
    replace hrly = 0 if `ihrly0' == 1
}

// Merge union membership and coverage into one categorical variable; create
// three categories (see value labels at bottom of this file).
generate byte union = 0 if !missing(job)
replace union = 1 if unionc == 1
replace union = 2 if unionm == 1
drop unionc unionm

// list duplicate jobs (they may / will differ w.r.t. other variables, though
duplicates report uuid wave refmth job sjdate ejdate occ ind

//------------------------------------------------------------------------------
// NUMBER OF WEEKS ON JOB 

// Compute monthly weeks on job (requires multjob to be defined!)
// Saved in variable wksjobi, imputation flag stored in flag_wksjob
do "sipp_set_d_job_weeks.do" `panel'

//------------------------------------------------------------------------------
// INDUSTRY AND OCCUPATION CLASSIFICATION

do "sipp_set_d_ind_occ.do" `panel'

//------------------------------------------------------------------------------
// EARNINGS AND WAGES
// run code related to tagging topcoded observations, etc.
do "sipp_set_d_wages.do" `panel'

// Impute type of work (employed, self-employed, both)
// This should in principle be equivalent to empled variable for the 90-93 panels, but 
// often the values of empled contradict presence/absence of earnings from employment / 
// self-employment. Therefore we impute this from earnings.
// Remember: selfinc1 and selfinc2 are identical for both job-month obs. for a
// geiven reference month
sort uuid wave refmth

gen wtype = .
by uuid wave refmth: replace wtype = 1 if (!missing(earn) | !missing(earn[_n-1]) | !missing(earn[_n+1])) ///
     & missing(selfinc1) & missing(selfinc2)
by uuid wave refmth: replace wtype = 2 if missing(earn) & missing(earn[_n+1]) & missing(earn[_n-1]) ///
     & (!missing(selfinc1) | !missing(selfinc2))
by uuid wave refmth: replace wtype = 3 if (!missing(earn) | ! missing(earn[_n+1]) | !missing(earn[_n-1])) ///
    & (!missing(selfinc1) | !missing(selfinc2))

//------------------------------------------------------------------------------
// PERSON-MONTH TAGS

// Create person-month tags in case we need to convert to person-month format,
// using only the primary job for multiple job holders.
// There is no need to collapse obs. for jobless and single-job workers, so for
// these obs. tag_pm = 1.

generate byte tag_pm = missing(job) | primjob == 1
format %1.0f tag_pm

// FIXME: drop in final version
// Checks whether we have Set D data for all obs. from Set A
merge m:1 uuid wave refmth using "`fn_set_a'", assert(match) nogenerate ///
    keepusing(wpfinwgt) nolabel
drop wpfinwgt

// ---------------------------------------------------------------------
// LABELS / NOTES 

label variable class "Class of worker"
label variable ersend "JW: Reason stopped working"
label variable esr "LF: employment status recode"
label variable estlemp "With same employer as previous wave"
label variable hrly "Paid by the hour this month"
label variable job "Job ID"
label variable primjob "Primary job indicator"
label variable rmwkwjb "LF: number of weeks with a job in month"
label variable selfinc1 "Self-employment income from business 1"
label variable selfinc2 "Self-employment income from business 2"
label variable union "Union or employee association member, or covered by union contract?"
label variable wksper "LF: number of weeks this reference month"
label variable emp "Employed at least one week in a month"
label variable hasjob "Has a job this month"
label variable wtype "Type of work: employed, self-employed or both"
label variable njobs_imp "Number of jobs this month (imputed)"
label variable wkhours "JW: Usual hours per week"
label variable multjob "Indicator variable: multiple-job holders"

label variable rwkesr1   "Employment Status Recode for Week 1"
label variable rwkesr2   "Employment Status Recode for Week 2"
label variable rwkesr3   "Employment Status Recode for Week 3"
label variable rwkesr4   "Employment Status Recode for Week 4"
label variable rwkesr5   "Employment Status Recode for Week 5"

label variable tag_pm "Tags one person-month obs. within each person-job-month group"
notes tag_pm: "Can be used to convert job-month to person-month format. For multiple job holders it is assured that only the primary job is tagged." 

if `panel' <= 1993 {
    label variable njobs "PM: No. of jobs this month"
    label variable empled "Type of work: employed, self-employed of both (only 90-93 panels)" 
}
if `panel' >= 1996 {
    label variable njobs "PM: No. of jobs this month (0=contingent worker)"
    label variable mooninc "Moonlighting income"
    label variable tempsiz "Number of employees at worker's location"
    label variable tempall "Number of employees at all locations"
}
if `panel' == 2004 | `panel' == 2008 {
    label variable hoursvar "Indicator variable: varying working hours"
}

notes njobs_imp: "Imputed from the number of non-missing jobids: either 0, 1 or 2 jobs, possibly overlapping"

// Dataset-specific notes
notes: All variables are monthly except where noted.
notes: All variables come from the longitudinal panel except where noted.

// ---------------------------------------------------------------------
// Label definitions and assignments

#delimit ;

label define lbl_emp 
    0   "In labor force, not employed all month"
    1   "Employed at least one week in a month" 
    ;
label define lbl_union 
    0   "Non-union, not covered by union contract" 
    1   "Non-union, covered by union contract"
    2   "Union or employee association member"
    ; 
label define lbl_ersend 
    1   "On layoff" 
    2   "Retirement or old age" 
    3   "Childcare problems" 
    4   "Other family/personal obligations" 
    5   "Own illness" 
    6   "Own injury" 
    7   "School/training" 
    8   "Discharged/fired" 
    9   "Employer bankrupt"
    10  "Employer sold business" 
    11  "Job was temporary and ended"
    12  "Quit to take another job" 
    13  "Slack work or business conditions"
    14  "Unsatisfactory work arrangements (hours, pay, etc)" 
    15  "Quit for some other reason"
    ;
label define lbl_wtype 
    1   "Worked for employer only" 
    2   "Self-employed only" 
    3   "Worked for both employer and self-employed"  
    ;
label define lbl_class 
    1   "Private for-profit" 
    2   "Private non-profit" 
    3   "Federal government" 
    4   "State government" 
    5   "Local government" 
    6   "Armed forces" 
    7   "Unpaid in familiy business or farm"
    ;
#delimit cr
label values class lbl_class

label values emp lbl_emp
label values union lbl_union

label values ersend lbl_ersend
notes ersend: For 90-93 reported categories are: 1 "Laid off" 2 "Retired" 8 "Discharged" 11 "Job was temporary and ended" 12 "Quit to take another job" 15 "Quit for some other reason"

label values wtype lbl_wtype

if `panel' >= 1996 {
    notes class: "Armed foces category (6) available only in 90-93 panels."
}

if `panel' >= 90 & `panel' <= 93 {
    #delimit ;
    label define lbl_njobs
        1   "Only one job" 
        2   "Dual jobs all month" 
        3   "Dual jobs not all month"
        4   "Dual jobs without overlapping dates" 
        5   "No job this month"
        ;
    #delimit cr

    label values njobs lbl_njobs
}

if `panel' == 2004 | `panel' == 2008 {
    label define hoursvar 1 "Hours vary" 0 "Hours do not vary"
    label values hoursvar hoursvar
}

//------------------------------------------------------------------------------
// CLEAN UP VARIABLES

// drop variables that logically belong to Set A
drop month rot year epppnum refdate

// drop other unneeded variables
drop wave_end wave_start

// drop wsjdate and wejdate, they exist only in 1990-1993 panels
capture drop wejdate wsjdate

// vim: sts=4 sw=4 et: 
