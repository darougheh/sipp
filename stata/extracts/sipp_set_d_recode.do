// Performs recodes of most variables for Set D to ensure consistent values /
// treatment of missing values across panels.
// This file should be -run- from sipp_process_d.do AFTER all longitudinal /
// monthly / wavely data has been merged.

// FIXME: CEPR copyright
// FIXME: licensing

version 12

// takes 4-digit panel year as argument
args panel

// recode all -1 to missing values
mvdecode _all, mv(-1)

// Stayed with employer from last wave
// CEPR extract originally claims this refers to last month, DDF clearly states it's the previous wave
recode estlemp (0 = .) (2 = 0)

recode wksper (min/0 = .)

if `panel' <= 1993 {
    // Main reason ... stopped working
    recode ersend (0 = .) (3 = 8) (4 = 11) (5 = 12) (6 = 15)
    // Employment status from longitudinal panel file
    recode esr (min/0 = .)

    // Beginning / end month of employment in 90-93 panels: 0 = Not in universe
    recode wsmonth wemonth (0 = .)
    recode empled (0 = .)
}

// Job Classification
if `panel' >= 1996  {
    recode class (5 = 3) (3 = 5) (6 = 7)
}
recode class (min/0 = .)

recode unionm unionc (0 = .)  (2 = 0)
recode hrly (0=.) (2=0)
recode rate earn selfinc* (min/0 = .)

// Mooning income for 1996+ panels
if `panel' >= 1996 {
    recode mooninc (0 = .)
}

recode ind* (0 = .)
recode occ* (0 = .)

// hours
// Consistent as always, the values for various panels are:
// 90-93: -3 None; 0 Not in universe / in sample / nonmatch; 1-99 hours
// 96/01: -1 not in universe; 1-99 hours
// 04/08: -8 hours vary; -1 not in universe; 1-99 hours
if `panel' <= 1993 {
    recode wkhours (-3 = 0) (0 = .)
}
else if `panel' == 1996 | `panel' == 2001 {
    recode wkhours (-1 = .)
}
else if `panel' == 2004 | `panel' == 2008 {
    // create indicator variable if hours vary in 2004+ panel
    generate byte hoursvar = 1 if wkhours == -8
    recode wkhours ( -8 = .)
}


// vim: sts=4 sw=4 et:
