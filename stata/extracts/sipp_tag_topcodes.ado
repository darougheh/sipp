// Tags earnings and hourly wage rate topcodes for 1990 - 2008 panels.
// Topcoded earnings are tagged using the EARNTAG indicator variable, topcoded
// hourly rates are tagged using WAGETAG variable. 
// For the 1996+ panels, topcoding also depends on whether a person was
// employed full time, full year. We cannot reliably reconstruct whether this is the
// case with public data, so an alternative method that disregards
// full time/full year potentially tags observations that were NOT topcoded
// originally. This alternative tag is indicted by in EARNTAG=2.
// Note that SIPP do not ensure any consistency between topcoded earnings and
// wages.

// This command should only be called from sipp_process_d.do, as we expect
// several variables from that data set to be present.

// Author: Richard Foltyn
// FIXME: licensing

capture program drop sipp_tag_topcodes
program define sipp_tag_topcodes

    version 12
    syntax, Panel(integer) WAGEtag(name) EARNtag(name)

    confirm new variable `wagetag' `earntag'
    generate byte `earntag' = 0 if !missing(earn)
    generate byte `wagetag' = 0 if !missing(rate)

    // For 1996+ panels, we cannot identify for sure whether earnings were topcoded as we cannot construct 
    // the working status reliably from the publicly available data. 
    // earntag=2 will potentially indentify earnings as topcoded even if they were not,
    // but it will not identify topcoded earnings as not topcoded, while `earntag' might.

    if `panel' >= 1996 {

        tempvar race

        // race relevant for topcoding
        generate `race' = 3 if eorigin >= 20 & eorigin <= 28        // Hispanic
        replace `race' = 2 if erace == 2 & missing(`race')    // Black
        replace `race' = 1 if missing(`race')       // non-black, non-Hispanic

        tempvar labforce fullty fulltime
        // worker status relevant for topcoding
        sort uuid job wave
        by uuid job wave: egen `labforce' = total(esr == 1)
        recode  `labforce' (1 2 3 = 0) (4 = 1)

        generate byte `fulltime' = 0
        // we cannot reproduce the criterion for "full time" as used for topcoding, because it
        // relies on weekly observations of hours worked, which we do not have.
        // Approximate by requireing that person must have worked 35+ hours in most of the weeks
        replace `fulltime' = 1 if rmhrswk == 1 | rmhrswk == 3 | rmhrswk == 4

        // indicator variable for full time, full year work
        generate byte `fullty' = `fulltime' & `labforce'
    }

    // post-1996 panels assigned mean values above topcode threshold to topcoded earnings,
    // depending on sex, race and working status

    if `panel' == 1996 {
        // Table 10-19 in the user guide
        replace `earntag' = 1 if earn == 29660 & female == 0 & `race' == 1 & `fullty' == 1
        replace `earntag' = 1 if earn == 38270 & female == 0 & `race' == 1 & `fullty' == 0 
        replace `earntag' = 1 if earn == 17530 & female == 0 & `race' == 2 & `fullty' == 1 
        replace `earntag' = 1 if earn == 24015 & female == 0 & `race' == 2 & `fullty' == 0 
        replace `earntag' = 1 if earn == 26250 & female == 0 & `race' == 3 & `fullty' == 1 
        replace `earntag' = 1 if earn == 24015 & female == 0 & `race' == 3 & `fullty' == 0 
        replace `earntag' = 1 if earn == 21990 & female == 1 & `race' == 1 & `fullty' == 1 
        replace `earntag' = 1 if earn == 49450 & female == 1 & `race' == 1 & `fullty' == 0 
        replace `earntag' = 1 if earn == 24015 & female == 1 & `race' == 2 & `fullty' == 1 
        replace `earntag' = 1 if earn == 24015 & female == 1 & `race' == 2 & `fullty' == 0 
        replace `earntag' = 1 if earn == 24015 & female == 1 & `race' == 3 & `fullty' == 1 
        replace `earntag' = 1 if earn == 24015 & female == 1 & `race' == 3 & `fullty' == 0 

        replace `earntag' = 2 if earn == 29660 & female == 0 & `race' == 1 
        replace `earntag' = 2 if earn == 38270 & female == 0 & `race' == 1 
        replace `earntag' = 2 if earn == 17530 & female == 0 & `race' == 2 
        replace `earntag' = 2 if earn == 24015 & female == 0 & `race' == 2 
        replace `earntag' = 2 if earn == 26250 & female == 0 & `race' == 3 
        replace `earntag' = 2 if earn == 24015 & female == 0 & `race' == 3 
        replace `earntag' = 2 if earn == 21990 & female == 1 & `race' == 1 
        replace `earntag' = 2 if earn == 49450 & female == 1 & `race' == 1 
        replace `earntag' = 2 if earn == 24015 & female == 1 & `race' == 2 
        replace `earntag' = 2 if earn == 24015 & female == 1 & `race' == 2 
        replace `earntag' = 2 if earn == 24015 & female == 1 & `race' == 3 
        replace `earntag' = 2 if earn == 24015 & female == 1 & `race' == 3 
    
        replace `wagetag' = 1 if rate == 29
    }
    else if `panel' == 2001 {
        // Table 10-22 in the user guide
        replace `earntag' = 1 if earn == 29057 & female == 0 & `race' == 1 & `fullty' == 1
	replace `earntag' = 1 if earn == 24956 & female == 0 & `race' == 1 & `fullty' == 0 
	replace `earntag' = 1 if earn == 20769 & female == 0 & `race' == 2 & `fullty' == 1 
	replace `earntag' = 1 if earn == 20769 & female == 0 & `race' == 2 & `fullty' == 0 
	replace `earntag' = 1 if earn == 24283 & female == 0 & `race' == 3 & `fullty' == 1 
	replace `earntag' = 1 if earn == 36866 & female == 0 & `race' == 3 & `fullty' == 0 
	replace `earntag' = 1 if earn == 23420 & female == 1 & `race' == 1 & `fullty' == 1 
	replace `earntag' = 1 if earn == 25973 & female == 1 & `race' == 1 & `fullty' == 0 
	replace `earntag' = 1 if earn == 26841 & female == 1 & `race' == 2 & `fullty' == 1 
	replace `earntag' = 1 if earn == 26841 & female == 1 & `race' == 2 & `fullty' == 0 
	replace `earntag' = 1 if earn == 31909 & female == 1 & `race' == 3 & `fullty' == 1 
	replace `earntag' = 1 if earn == 31909 & female == 1 & `race' == 3 & `fullty' == 0 

        replace `earntag' = 2 if earn == 29057 & female == 0 & `race' == 1 
	replace `earntag' = 2 if earn == 24956 & female == 0 & `race' == 1 
	replace `earntag' = 2 if earn == 20769 & female == 0 & `race' == 2 
	replace `earntag' = 2 if earn == 20769 & female == 0 & `race' == 2 
	replace `earntag' = 2 if earn == 24283 & female == 0 & `race' == 3 
	replace `earntag' = 2 if earn == 36866 & female == 0 & `race' == 3 
	replace `earntag' = 2 if earn == 23420 & female == 1 & `race' == 1 
	replace `earntag' = 2 if earn == 25973 & female == 1 & `race' == 1 
	replace `earntag' = 2 if earn == 26841 & female == 1 & `race' == 2 
	replace `earntag' = 2 if earn == 26841 & female == 1 & `race' == 2 
	replace `earntag' = 2 if earn == 31909 & female == 1 & `race' == 3 
	replace `earntag' = 2 if earn == 31909 & female == 1 & `race' == 3 

        replace `wagetag' = 1 if rate == 29
    }
    else if `panel' == 2004 {
        // Table 10-23 in the user guide
        replace `earntag' = 1 if earn == 37750 & female == 0 & `race' == 1 & `fullty' == 1
	replace `earntag' = 1 if earn == 38900 & female == 0 & `race' == 1 & `fullty' == 0 
	replace `earntag' = 1 if earn == 51400 & female == 0 & `race' == 2 & `fullty' == 1 
	replace `earntag' = 1 if earn == 51400 & female == 0 & `race' == 2 & `fullty' == 0 
	replace `earntag' = 1 if earn == 33600 & female == 0 & `race' == 3 & `fullty' == 1 
	replace `earntag' = 1 if earn == 33600 & female == 0 & `race' == 3 & `fullty' == 0 
	replace `earntag' = 1 if earn == 30000 & female == 1 & `race' == 1 & `fullty' == 1 
	replace `earntag' = 1 if earn == 43500 & female == 1 & `race' == 1 & `fullty' == 0 
	replace `earntag' = 1 if earn == 51400 & female == 1 & `race' == 2 & `fullty' == 1 
	replace `earntag' = 1 if earn == 51400 & female == 1 & `race' == 2 & `fullty' == 0 
	replace `earntag' = 1 if earn == 33600 & female == 1 & `race' == 3 & `fullty' == 1 
	replace `earntag' = 1 if earn == 33600 & female == 1 & `race' == 3 & `fullty' == 0 

        replace `earntag' = 2 if earn == 37750 & female == 0 & `race' == 1 
	replace `earntag' = 2 if earn == 38900 & female == 0 & `race' == 1 
	replace `earntag' = 2 if earn == 51400 & female == 0 & `race' == 2 
	replace `earntag' = 2 if earn == 51400 & female == 0 & `race' == 2 
	replace `earntag' = 2 if earn == 33600 & female == 0 & `race' == 3 
	replace `earntag' = 2 if earn == 33600 & female == 0 & `race' == 3 
	replace `earntag' = 2 if earn == 30000 & female == 1 & `race' == 1 
	replace `earntag' = 2 if earn == 43500 & female == 1 & `race' == 1 
	replace `earntag' = 2 if earn == 51400 & female == 1 & `race' == 2 
	replace `earntag' = 2 if earn == 51400 & female == 1 & `race' == 2 
	replace `earntag' = 2 if earn == 33600 & female == 1 & `race' == 3 
	replace `earntag' = 2 if earn == 33600 & female == 1 & `race' == 3 


        replace `wagetag' = 1 if rate == 28.5
    }
    else if `panel' == 2008 {
        // these numbers were obtained by email from the US Census Bureau as they are not yet
        // published (as of June 2012).
        replace `earntag' = 1 if earn == 30200 & female == 0 & `race' == 1 & `fullty' == 1
	replace `earntag' = 1 if earn == 30000 & female == 0 & `race' == 1 & `fullty' == 0 
	replace `earntag' = 1 if earn == 31800 & female == 0 & `race' == 2 & `fullty' == 1 
	replace `earntag' = 1 if earn == 57900 & female == 0 & `race' == 2 & `fullty' == 0 
	replace `earntag' = 1 if earn == 22000 & female == 0 & `race' == 3 & `fullty' == 1 
	replace `earntag' = 1 if earn == 26100 & female == 0 & `race' == 3 & `fullty' == 0 
	replace `earntag' = 1 if earn == 31900 & female == 1 & `race' == 1 & `fullty' == 1 
	replace `earntag' = 1 if earn == 38500 & female == 1 & `race' == 1 & `fullty' == 0 
	replace `earntag' = 1 if earn == 23400 & female == 1 & `race' == 2 & `fullty' == 1 
	replace `earntag' = 1 if earn == 57900 & female == 1 & `race' == 2 & `fullty' == 0 
	replace `earntag' = 1 if earn == 26100 & female == 1 & `race' == 3 & `fullty' == 1 
	replace `earntag' = 1 if earn == 26100 & female == 1 & `race' == 3 & `fullty' == 0 

        replace `earntag' = 2 if earn == 30200 & female == 0 & `race' == 1 
	replace `earntag' = 2 if earn == 30000 & female == 0 & `race' == 1 
	replace `earntag' = 2 if earn == 31800 & female == 0 & `race' == 2 
	replace `earntag' = 2 if earn == 57900 & female == 0 & `race' == 2 
	replace `earntag' = 2 if earn == 22000 & female == 0 & `race' == 3 
	replace `earntag' = 2 if earn == 26100 & female == 0 & `race' == 3 
	replace `earntag' = 2 if earn == 31900 & female == 1 & `race' == 1 
	replace `earntag' = 2 if earn == 38500 & female == 1 & `race' == 1 
	replace `earntag' = 2 if earn == 23400 & female == 1 & `race' == 2 
	replace `earntag' = 2 if earn == 57900 & female == 1 & `race' == 2 
	replace `earntag' = 2 if earn == 26100 & female == 1 & `race' == 3 
	replace `earntag' = 2 if earn == 26100 & female == 1 & `race' == 3 

        replace `wagetag' = 1 if rate == 35.0
    }

    // Catchall topcoding algorithm:
    // Note that we cannot solely rely on this, as it would not detect some topcoded wages, e.g.
    // if someone has very high income in only one month of the wave, it is most likely topcoded to
    // something below 50,000 (66,666 for later panels), so wavely earnings would not exceed threshold.

    if `panel' >= 1996 {
        
        tempvar earn_wave

        sort uuid job wave refmth
        // compute wavely earnings
        by uuid job wave: egen `earn_wave' = total(earn)

        // wave earnings threshold seems to be at 66666 for the 04/08 panels
        // even if the user guide does not say so.
        local threshold = cond(`panel' == 1996 | `panel' == 2001, 50000, 66666)
        replace `earntag' = 2 if `earn_wave' > `threshold' & !missing(earn)
    }

    // pre-1996 panels simply cap reported earnings at 33,332 per wave.
    // If this threshold is reached monthly earnings are capped at 8,333

    if `panel' >= 1990 & `panel' <= 1993 {
        replace `earntag' = 2 if earn == 8333 | earn == 33332 
    }



    #delimit ;
    label define lbl_`earntag'
        0   "Not topcoded"
        1   "Earnings topcoded, using imputed full year/full time (96-08)"
        2   "Earnings topcoded, ignoring full year/full time"
        3   "Imputed from topcoded wage rate"
        ;
    label define lbl_`wagetag'
        0   "Not topcoded"
        1   "Wage rate topcoded"
        3   "Imputed from topcoded earnings"
        ;
    #delimit cr

    label values `earntag' lbl_`earntag'
    label values `wagetag' lbl_`wagetag'

    label variable `earntag' "Flag indicating earnings topcode"
    label variable `wagetag' "Flag indicating hourly rates topcode"
    format %1.0f `earntag' `wagetag'

end // sipp_tag_topcodes

// vim: set sts=4 sw=4 et:
