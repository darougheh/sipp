

// Author: Richard Foltyn
// FIXME: licensing

capture program drop sipp_dates_to_weeks
program define sipp_dates_to_weeks

version 12
syntax [if] [in], generate(name) [sdate(varname)] [edate(varname)]

local wksemp: copy local generate
confirm new variable `wksemp'

local sdate = cond("`sdate'" == "", "wsjdate", "`sdate'")
local edate = cond("`edate'" == "", "wejdate", "`edate'")

confirm variable `sdate' `edate' refdate

marksample touse

tempvar ms me valid

// calculate dates of first and last day of month
generate `ms' = dofm(refdate)
generate `me' = dofm(refdate + 1) - 1

tempvar jmstart jmend

generate `jmstart' = max(`sdate', `ms')
generate `jmend' = min(`edate', `me')

generate `wksemp' = (`jmend' - `jmstart' + 1) / 7 if `touse' & !missing(job)

// remove nonsensical results due to conflicting start/end dates
replace `wksemp' = 0 if `wksemp' < 0

end // sipp_dates_to_weeks

// vim: sts=4 sw=4 et:
