// Suppresses trace output even if trace is set to 'on'. The trace level is
// restored after the command passed as an argument is executed.

// Author: Richard Foltyn
// FIXME: licensing

/*
Usage: prefix any command with dbg_off, e.g.
    
    dbg_off clear

to supress tracing output from this particular command.
dbg_off can be freely combined with -quiet- to further minimize verbosity,
either as

    dbg_off quiet foo

or

    quiet dbg_off foo

WARNING: Do NOT use in combination with -include-, as then all local macros
from the original context are undefined within the included file, defeating the
idea behind -include- (as opposed to -do- and -run-).
    
*/ 

capture program drop dbg_off
program define dbg_off

    local trc = c(trace)
    set trace off

    // execute whatever was passed as a parameter
    `*'

    set trace `trc'

end // dbg_off

// vim: set sts=4 sw=4 et:
