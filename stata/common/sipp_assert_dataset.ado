// Assets that the dataset passed as the first (and only) argument is one of
// the valid strings 'core', 'tm', 'fp', 'jid' or 'wgt'.

// Author: Richard Foltyn
// FIXME: licensing

capture program drop sipp_assert_dataset.ado
program define sipp_assert_dataset

    args ds_name

    local ds_name = lower("`ds_name'")

    local ds_name_valid = "core tm fp jid wgt"
    local is_valid: list ds_name in ds_name_valid

    if `is_valid' == 0 {
        display as error "Invalid data set type. Must be one of `ds_name_valid'"
        exit 9 
    }
end

// vim: set sts=4 sw=4 et:
