// Runs some diagnostics to check processed SIPP data.

// Author: Richard Foltyn

set more off
set tracedepth 2
set trace on
discard

// set up global variables
include "diag_env.do"

diag_universe, saveas("${graph_dir}/diag_pop.eps")


set trace off
