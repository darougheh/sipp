// Program to import full panel data (for 1990-1993 panels) from NBER 
// raw files to Stata dta format.
// Author: Richard Foltyn
// FIXME: licensing

capture program drop sipp_nber_import_fp
program define sipp_nber_import_fp

syntax, Panel(integer)

// assert whether this is a valid panel id
dbg_off sipp_assert_panel `panel'

if `panel' < 1990 | `panel' > 1993 {
    display as error "Invalid panel. Full panel data available only for 1990..1993"
    // error 125
    exit
}

// need to clear explicitly, otherwise there might be some labels left from
// previous runs of import programs.
dbg_off clear

dbg_off sipp_nber_file_names, panel(`panel') dataset("fp")

quietly infile using "${nber_dict_dir}/`r(dct)'", using("${nber_raw_dir}/`r(raw)'")
dbg_off run "${nber_do_dir}/`r(lbl)'"

sipp_gen_id, panel(`panel') dataset("fp") generate(uuid)

sipp_apply_revisions, panel(`panel') dataset("fp")

run "sipp_custom_formats.do"
run "sipp_custom_labels.do"

sort uuid
    
// retrieve 'canonical' file name for output file
sipp_gen_fname, panel(`panel') dataset("fp")
save "${nber_out_dir}/`r(fname)'", replace

end // sipp_nber_import_fp


