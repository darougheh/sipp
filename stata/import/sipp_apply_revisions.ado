// Apply data revisions to published in the SIPP User Notes on 
// http://www.census.gov/sipp/notes.html to SIPP data from NBER.

// See the file revisions/README.md on how these revisions are applied.

// Author: Richard Foltyn
// FIXME: licensing

capture program drop sipp_apply_revisions
program define sipp_apply_revisions

// pass all arguments to actually do-files that perform data revisions
syntax, Panel(passthru) DATaset(passthru) [Wave(passthru)]


local rev_files: dir "${sipp_revisions_dir}" files "*.do"
local rev_files: list sort rev_files

foreach f of local rev_files {

    local lrun = regexm("`f'", "^[0-9]+-.*[.]do$")

    if `lrun' == 1 {
        // run file first to update definition of program 'sipp_apply_rev'
        run "${sipp_revisions_dir}/`f'"

        sipp_apply_rev, `panel' `dataset' `wave'
    }

}



end // sipp_apply_revisions

// vim: set sts=4 sw=4 et:
