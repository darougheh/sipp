// Global paths for data, program, etc. directories

// get home directory
local home: environment HOME
local dataroot = "/Data/SIPP"

// base directory of the git repository containing all SIPP code
local git_repo = "`home'/repos/sipp"

global log_dir = "`home'/stata/log"


// ------------------------------------------------------------
// PROGRAM DIRECTORIES
// Add ado-files in common/ to ADO path so we do not need to prefix them with
// directory.

adopath + "`git_repo'/stata/common"


// ------------------------------------------------------------
// NBER import directories

// directory containing Stata dictionary files (*.dct) needed to import raw
// data files
global nber_dict_dir = "`git_repo'/stata/import/dict"

// directory containing Stata do-files that assign labels to imported raw data.
global nber_do_dir = "`git_repo'/stata/import/labels"

// directory containing the raw data files obtained from www.nber.org/sipp
// Note that you have to unzip these yourself
global nber_raw_dir = "`dataroot'/raw"

// directory where processed stata *.dta files will be stored
global nber_out_dir = "`dataroot'/imported"

// ------------------------------------------------------------
// SIPP data revisions

// directory containing do files to apply SIPP data revisions which were not
// incorporared into re-released data files.
global sipp_revisions_dir = "`git_repo'/stata/import/revisions"
