// Fixes problems with educational attainment in 2004 panels.
// For details, see the SIPP user notes at
// http://www.census.gov/sipp/core_content/core_notes/panel_2004_EDUC_error_memo.pdf

// Note that according to information on the SIPP mailing lists, these errors
// were not fixed by later re-releases of the affected wave files mentioned in 
// http://www.census.gov/sipp/core_content/core_notes/2008w2_5-2004w2_12-Edu-Attain-User-Note.html

// Author: Richard Foltyn
// FIXME: licensing

capture program drop sipp_apply_rev
program define sipp_apply_rev

    syntax , Panel(integer) DATaset(string) [Wave(integer 0)]

    if `panel' != 2004 | `"`dataset'"' != "core" {
        exit
    }

    replace eeducate = 38 if eeducate == 41 & rcollvoc == 1
    replace eeducate = 39 if eeducate == 41 & rcollvoc == 2
    replace eeducate = 40 if eeducate == 41 & (rcollvoc == 3 | rcollvoc == 4)
    replace eeducate = 41 if eeducate < 41 & rcollvoc >= 6 & rcollvoc <= 9

end

// vim: set sts=4 sw=4 et:
