#!/bin/bash

# Convenience wrapper to run sipp-fix-nber-*.sed on multiple files and save
# output to a user-specified directory.

# Author: Richard Foltyn
# License: GPLv2 or later

SED_SCRIPT="$(dirname $0)/$(basename $0 .sh).sed"

. "$(dirname $0)/sed-common.sh"
