#!/bin/sed -nrf 

# Author: Richard Foltyn
# FIXME: license

# See README.md in the directory stata/import/dict/ for a comprehensive list of
# changes applied to NBER dictionary files.

:lbl1
# Convert all blank lines with some whitespace to true blank lines, then
# collapse multiple consecutive blank lines into one.
s/^[[:space:]]+$/\n/Mg
s/\n{3,}/\n\n/g

# Process comments starting with ** (Copyright comments in later dict files)
/[*]{2,}.*((\n[ \t]*[^*\n]{2,}[^\n]*)|(@@@@@))/ {
    # keep comment block only if it contains copyright
    /[cC]opyright/! d
    # In some files, copyright notice has " ;" at every line end
    s/[[:space:]]*;$//Mg
    # drop blank lines right before EOF
    s/[\n]+(@@@@@)$/\n\1/
    # replace ** with * at beginning of line
    s/^[ \t]*[*]{2,}/*/Mg
    # comment out sandwiched blank lines too
    s/^[ \t]*$/*/Mg
    s/^/\n/
    # delete marker
    s/@@@@@//
    p; blblend
}
/[*]{2,}.*/ {
    # use some cheap trick to identify EOF in the pattern above, will delete
    # pattern afterwards.
    $ s/$/\n@@@@@/
    $!N;
    blbl1 
}

:lblc
# Process copyright comments that are wrapped in /* */ (earlier dict files)
\|/\*.*copyright.*\*/|I { 
    # In some files, copyright notice has " ;" at every line end
    s/[[:space:]]*;$//Mg
    # delete initial /*
    s|[[:space:]]*/\*[[:space:]]*[\n]*||
    # delete terminating */
    s|[[:space:]]*\*/[[:space:]]*\n*||
    # comment out all lines that do not already start with *
    s/^([^*])/* \1/Mg
    # comment out sandwiched blank lines too
    s/^[ \t]*$/*/Mg
    s/^/\n/
    p;
    blblend 
}
# delete all other /* */ comments (there should be none, though)
s|/\*.*\*/||g
/\/\*/N
//blblc

# delete empty lines outside of block comments
/^[[:space:]]*$/ d

# uncomment commented-out data fields (which were commented due to restrictions
# on the number of variables of some Stata versions)
s/^[[:space:]]*\*#[[:space:]]*(.*)$/\1/M

# delete all other single-line comments with one leading *
/^[[:space:]]*\*[^*]+.*$/ d

# Remove hard-coded file names of raw data files
/(infile[[:space:]]+)?[[:space:]]*dictionary[^{]*\{/ {
    s/^[^{]*\{/infile dictionary {/
    # this is supposed to be the very first line in the dictionary file (no
    # comments are allowed prior to this line!), so print it right away
    p; b
}

# Changing ID-type variables from str* to byte/integer/long/double
# The variable types in the NBER dictionaries are not very consistent; we
# change all (relevant) ID-like variables to numeric format. The numeric format
# depends on the number of digits, e.g.
#  str2 -> byte
#  str[34] -> int
#  str[5-9] -> long
#  str12 -> double
s/[[:space:]]*str5[[:space:]]*suseqnum([[:space:]]*)%5s/   long suseqnum\1%5f/I

# change entry address IDs in pre-1996 panels to byte.
s/[[:space:]]*str2[[:space:]]*(entry|pp_entry)([[:space:]]*)%2s/   byte \1\2%2f/I

# There are some additional fields containing address ids in pre-1996 panels,
# e.g. h5addid, pwaddid; changes there so byte as well
# In 1992 full panel, hh_add* is str3, so convert to int
s/[[:space:]]*str[23][[:space:]]*([[:alnum:]]*addid|hh_add[[:digit:]]*)([[:space:]]*)%([23])s/    int \1\2%\3f/I

# These variables are either 3- or 4-digit numbers, import as int
s/[[:space:]]*str[34][[:space:]]*(eentaid|epppnum|pnum|pp_pnum|pp_entry)([[:space:]]*)%([34])s/    int \1\2%\3f/I 
# SUIDs in pre-1996 panels are 9-digit numbers
s/[[:space:]]*str9[[:space:]]*(id|suid|pp_id)([[:space:]]*)%([0-9]*)[[:alpha:]]/   long \1\2%\3f/I

# SSUID in 1996+ panels has 12 digits, this no longer fits into long
s/[[:space:]]*str12[[:space:]]*ssuid([[:space:]]*)%12s/ double ssuid\1%12f/I

# Some 1992 full panel-specific fixes:
# Make sure naming is consistent across files: 1992 panel has pp_int, while all
# others have pp_intv.
s/\bpp_int([0-9]{2,})/pp_intv\1/

# rename various variables so they correspond to names in DDF files
s/\batt_sch?([0-9]+)/att_schl\1/
s/\bgrd_cmp?([0-9]+)/grd_cmpl\1/
s/\bed_leve?([0-9]+)/ed_level\1/
s/\bhigrad([0-9]+)/higrade\1/
s/\bgeo_st([0-9]+)/geo_ste\1/
s/\benrl_m([0-9]+)/enrl_mth\1/

# job-related stuff to rename
s/\bwksper([0-9]+)/wkspermn\1/
s/\bclswk([0-9]+)/clsswrk\1/
s/\bernam([0-9]+)/ern_amt\1/
s/\bhrrat([0-9]+)/hrlyrat\1/
s/\bse_am([0-9]+)/se_amt\1/
s/\bmthjbw([0-9]+)/mthjbwks\1/
s/\bmthwop([0-9]+)/mthwopwk\1/

# remove trailing _ in some variable names in full panel files, as most other
# variables do not use _ to separate variables from waves.
s/\b(age|ms|esr)_([0-9]+)/\1\2/



# if we actually got to this point this is a valid line, save it to hold space
# and print it out after infile declaration and copyright comment.
H

:lblend
$ { g; p }
