#!/bin/bash

# Author: Richard Foltyn
# License: GPLv2 or later

# Common code to apply a sed script to a arbitrary number of files, either
# overwriting them in place of writing the processed files to a directory
# passed by the -d option.
# The sed script has to be specified in the SED_SCRIPT variable prior to
# sourcing this script.

. $(dirname $0)/sipp-common.sh

print_usage() {
    cat << EOF

Usage: $(basename $0) [OPTIONS] file1 [file2 file3 ... ]

Valid OPTIONS:
    -d  Output directory for processed files
        (otherwise files are overwritten in place)
    -v  Verbose output
EOF
}

if [ -z "$1" ]; then
    print_usage
    exit 1
fi

while getopts "d:v" OPTION
do
    case $OPTION in 
        d)  OPT_OUTDIR="${OPTARG%/}"
            assert_dir_writable "$OPT_OUTDIR"
            ;;
        v)  OPT_VERBOSE=1
            ;;
        *)  print_usage
            exit 1
            ;;
    esac
done

shift $(($OPTIND -1))

[ -n "$*" ] || die "No files specified!"

for file in $@; do
    echo_cond "$OPT_VERBOSE" "Processing $file ..."
    if [ -z "$OPT_OUTDIR" ]; then
        "$SED_SCRIPT" -i "$file"
    else
        "$SED_SCRIPT" < "$file" > "$OPT_OUTDIR/$(basename $file)"
    fi
done

# vim: set sw=4 sts=4 et:
